<?php
/*
Plugin Name: Disable Repeat Purchase Virtual Products- WooCommerce
Description: Disable the ability for logged in users to purchase items they already own that are virtual. It checks if the product has been purchased and is available to buy virtual product on their account.
Author: maioance
Author URI: 
Version: 1.0
Text Domain: disable-virtual-product-repeat-purchase
License: GPLv2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

function products_bought_by_curr_user() {
   
    // GET CURR USER
    $current_user = wp_get_current_user();
    if ( 0 == $current_user->ID ) return;
   
    // GET USER ORDERS (COMPLETED + PROCESSING)
    $customer_orders = get_posts( array(
        'numberposts' => -1,
        'meta_key'    => '_customer_user',
        'meta_value'  => $current_user->ID,
        'post_type'   => wc_get_order_types(),
        'post_status' => array_keys( wc_get_is_paid_statuses() ),
    ) );
   
    // LOOP THROUGH ORDERS AND GET PRODUCT IDS
    if ( ! $customer_orders ) return;
	$product_ids = array();
    foreach ( $customer_orders as $customer_order ) {
		$order = wc_get_order( $customer_order->ID );
        $items = $order->get_items();
        foreach ( $items as $item ) {
			$product_id = $item->get_product_id();
            $product_ids[] = $product_id;
        }
    }
    $product_ids = array_unique( $product_ids );
    $product_ids_str = implode( ",", $product_ids );
   
    // PASS PRODUCT IDS TO PRODUCTS SHORTCODE
    return $product_ids;
}

function som_disable_repeat_purchase( $purchasable, $product ) {

	// Get the ID for the current product
	$product_id = $product->id;
	$user_products_ids = products_bought_by_curr_user();
	// return false if the customer has bought the product and is currently available for buy
	if ( wc_customer_bought_product( get_current_user()->user_email, get_current_user_id(), $product_id ) && ($product->virtual == 'yes') ) {

		if ($user_products_ids) {
			foreach ($user_products_ids as $user_product_id) :
				$product = wc_get_product($user_product_id);
				if ($product->virtual == 'yes') {
					$purchasable = false;
				}
			endforeach;
		}
	}
	return $purchasable;
}
add_filter( 'woocommerce_is_purchasable', 'som_disable_repeat_purchase', 10, 2 );